///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 06c - countdown - EE 205 - Spr 2022
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date. This one's counting up from
//   the Fourth of July! :D
//
// Example:
//   Reference time: Wed Jul 04 12:00:01 AM HST 2001
//   Years: 20 Days: 237 Hours: 2 Minutes: 49  Seconds:  58
//   Years: 20 Days: 237 Hours: 2 Minutes: 49  Seconds:  59
//   Years: 20 Days: 237 Hours: 2 Minutes: 50  Seconds:  0
//
// @author Rachel Watanabe <rkwatana@hawaii.edu>
// @date   25_02_2022
///////////////////////////////////////////////////////////////////////////////
#include <time.h>
#include <stdio.h>
#include <unistd.h>

#define REF_YEAR (2001)
#define REF_MONTH (07)
#define REF_DAY (04)
#define SEC_IN_MIN (60)
#define SEC_IN_HOUR (3600)
#define SEC_IN_DAY (86400)
#define SEC_IN_MONTH (2629743) //average 30.44 days
#define SEC_IN_YEAR (31556926) //average 365.24 days

void printDifference(double difference);

int main() {
   struct tm ref;
      ref.tm_year = REF_YEAR - 1900;
      ref.tm_mon  = REF_MONTH - 1;
      ref.tm_mday  = REF_DAY;
      ref.tm_hour = 0;
      ref.tm_min  = 0;
      ref.tm_sec  = 1;

   time_t reference = mktime(&ref);
   char printer[80];
   
   strftime(printer,sizeof(printer),"%a %b %d %r %Z %G", &ref);
   printf("Reference time: %s\n", printer);
   
   while(1) {   
      time_t current = time(NULL);
      double difference = difftime(current, reference);
 
      printDifference(difference);

      sleep(1);

      }
}

//sorry this is down here :(
void printDifference(double difference) {

   int years = difference / SEC_IN_YEAR;
   int days =(difference - (years * SEC_IN_YEAR))/ SEC_IN_DAY;
   int hours = (difference - (years * SEC_IN_YEAR) - (days * SEC_IN_DAY)) / SEC_IN_HOUR;
   int minutes = (difference - (years * SEC_IN_YEAR) - (days * SEC_IN_DAY) - (hours * SEC_IN_HOUR)) / SEC_IN_MIN;
   int seconds = difference - (years * SEC_IN_YEAR) - (days * SEC_IN_DAY) - (hours * SEC_IN_HOUR) - (minutes * SEC_IN_MIN);

   printf("Years: %d ", years);
   printf("Days: %d ", days);
   printf("Hours: %d ", hours);
   printf("Minutes: %d  ", minutes);
   printf("Seconds:  %d\n", seconds);

}
